# Reddit Comment Tools
Available tools include:

- `pull_comments.py`: Allows you to pull comments from any subreddit between a specified study period. Each comment will come with these columns below in the dataframe. The comments pulled will be exported to the `raw` directory.

```
author
body
created_utc
id
is_submitter
parent_id
permalink
retrieved_on
stickied
subreddit
subreddit_id
score
locked
link_id
```

- `clean_comments.ipynb`: Allows you to add a datetime column and export to the `clean` directory. Will also allow you to export samples to the `samples` directory. And finally will remove comments outside of the study period, duplicates, removed/deleted comments, bots as specified in `known_bots.txt`, and `NaN` values.

- `descriptive_analysis.ipynb`: Does some rudimentary analysis and figures on the raw or cleaned data. Figures will be exported to the `figures` directory.

- `vader.py`: Loops through all the rows and adds VADER scores to all the comments and exports to the `vader` directory.

- `vader_analysis.ipynb`: Allows you to plot some of the information produced from `vader.py`. These figures will be exported to the `figures` directory.