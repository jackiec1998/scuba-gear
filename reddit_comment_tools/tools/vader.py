from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import pandas as pd
import csv
from tqdm import tqdm
import time

start = time.time()

df = pd.read_csv("../text_only/live_threads.csv", index_col=0, parse_dates=['created_date'])

print(df.shape)

analyser = SentimentIntensityAnalyzer()

for index, row in tqdm(df.iterrows()):

    scores = analyser.polarity_scores(row['body'])

    df.loc[index, 'pos'] = scores['pos']
    df.loc[index, 'neu'] = scores['neu']
    df.loc[index, 'neg'] = scores['neg']
    df.loc[index, 'compound'] = scores['compound']


df.to_csv("../vader/live_threads.csv", quoting=csv.QUOTE_NONNUMERIC)

print(time.time() - start)
