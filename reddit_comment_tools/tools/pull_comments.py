import sys
import requests
import json
import time
import datetime as dt
import pandas as pd
import os
import time
import csv
import pytz

'''
    Included functions:
        - helper: send_request()
        - helper: save()
        - helper: format_time()
        - helper: request_loop()
        - helper: process_request()
        - main: pull_comments()

    Can just fiddle with pull_comments() at the end of the file. The documentation
    for the parameters are only listed for the main function, pull_comments().
'''

'''
    Description: Sends a singular request to Pushshift's API comment endpoint.
'''
def send_request(start, end, subreddit, additional_params):

    PUSHSHIFT_COMMENT_ENDPOINT = "http://api.pushshift.io/reddit/comment/search/"

    params = {
        "after": start,
        "before": end,
        "subreddit": subreddit,
        "sort_type": "created_utc",
        "sort": "asc",
        "size": 100 # That's the maximum size, default is lower.
    }

    if additional_params is not None:
        params.update(additional_params)

    # Print out request information.
    print(params)

    date = dt.datetime.fromtimestamp(start) \
        .strftime('%Y-%m-%d, %H:%M:%S')

    print(f"Comments after {date} requested.")

    request = requests.get(PUSHSHIFT_COMMENT_ENDPOINT, params=params, timeout=60)

    if request.status_code == 200:
        response = json.loads(request.text)
        return response['data']
    
    else:
        print("Request was unsuccessful. Error code below.")
        print(request.status_code)
        return None

'''
    Description: Saves the cache dataframe into the main dataframe and exports
        to CSV.
'''
def save(main_df, cache_df, file_name):

    if not cache_df.empty:
        main_df = main_df.append(cache_df, ignore_index=True)

        main_df = main_df.drop_duplicates()

        # Quoting all non-numeric values to avoid issues with carriage returns
        # (\r) and newlines (\n).
        main_df.to_csv(file_name, quoting=csv.QUOTE_NONNUMERIC)

        return main_df
    
    else:
        print("Given cache dataframe was empty. Please check the code. " \
            + "The CSV/main dataframe will stay the same.")
        
        return main_df

'''
    Description: Formats the time because I'm annoyed about doing it.
'''
def format_time(start_time):
    return f"{time.time() - start_time:.2f}s "

'''
    Description: Sends requests until successful or runs out of retries.
'''
def request_loop(start, end, subreddit, additional_params, max_retries):

    comments = None
    retries = 0

    # Loop requests until we run out of retries or successful.
    while (comments is None or len(comments) == 0) and retries <= max_retries:

        if retries > 0: print(f"Retry {retries}!")

        comments = send_request(start, end, subreddit, additional_params)

        retries += 1

    # Check if we pulled any comments from the API.
    if comments is None or len(comments) == 0:

        print("The requests returned nothing and ran out of retries." \
            + "Check request error. Flagging error and writing to CSV and exiting.")

        return None, False

    return comments, True

'''
    Description: Goes through the request loop and processes the comments into
        the cache.
'''
def process_request(main_df, cache_df, start, end, subreddit, file_name, max_retries,
    sleep_duration, cache_limit, additional_params, desired_columns):

    # Start off where you left off either in the cache or main dataframe.
    if not cache_df.empty:
        start = int(cache_df['created_utc'].max())
    elif not main_df.empty:
        start = int(main_df['created_utc'].max())

    request_time = time.time()

    comments, pull_successfully = request_loop(start, end, subreddit,
        additional_params, max_retries)

    if not pull_successfully:
        save(main_df, cache_df, file_name)
        sys.exit()

    print(f"{len(comments)} / " + format_time(request_time) \
        + "objects returned/response time.")

    formatting_time = time.time()

    for comment in comments:

        # Keep the columns you want only.
        comment = {key: comment[key] for key in desired_columns}

        cache_df = cache_df.append(comment, ignore_index=True)

    # Check if you want to clear the cache.
    if cache_df.shape[0] > cache_limit:

        print("--- Cache limit hit. Writing to CSV file and emptying cache. ---")

        main_df = save(main_df, cache_df, file_name)

        cache_df = pd.DataFrame()

    print(f"{cache_df.shape[0]} / {main_df.shape[0]} comments in cache/main.")

    print(format_time(formatting_time) + "for formatting.")

    print(f"{sleep_duration}s for sleeping.\n")
    time.sleep(sleep_duration)

    return main_df, cache_df
    

'''
    Description: Starts the comment pulling process by calling a hierarchy of
        arguably unnecessary helper functions that make it somewhat more
        readable/modular.

    Necessary Parameters:
        - start (int): Specifying in Unix epoch the beginning of the study period.
        - end (int): Specifying in Unix epoch the end of the study period.
        - subreddit (string): A string representing the desired subreddit.
        - file_name (string): The name of the file to import/export CSV from/to.

    Optional Parameters:
        - max_retries=5 (int): The number of retries to send to Pushshift before
            gracefully stopping.
        - sleep_duration=0.5 (float): The number of seconds to wait after each
            request.
        - cache_limit=9_999 (int): The number of comments in the cache before
            emptying to the main dataframe. It's a balancing act.
        - additional_params=None (dict): Additional parameters to append to the
            request. Look at documentation here: https://pushshift.io/api-parameters/
        - new_file=False (boolean): If flagged true, then it will delete existing
            work and begin the request again.
'''
def pull_comments(start, end, subreddit, file_name, max_retries=5,
    sleep_duration=0.5, cache_limit=9_999, additional_params=None, new_file=False):

    with open("../desired_columns.txt") as file:
        desired_columns = file.read().splitlines()
    
    # Check if ../raw/ directory exists, if not, create it.
    raw_directory = "../raw"
    if not os.path.isdir(raw_directory):
        os.mkdir(raw_directory)

    # Check if the given file_name ends with ".csv"
    if not file_name.endswith(".csv"):
        print("File name must end with \".csv\" to be valid. Exiting.")
        sys.exit()
    
    file_name = os.path.join(raw_directory, file_name)

    # Start a new_file if that is flagged.
    if new_file and os.path.isfile(file_name):
        os.remove(file_name)

    # Importing or creating the main dataframe.
    if os.path.isfile(file_name):
        main_df = pd.read_csv(file_name, index_col=0)
    else:
        main_df = pd.DataFrame()

    cache_df = pd.DataFrame()

    # Loop that actually does the heavy lifting.
    while True:

        try:

            # Holy cow this looks ugly. This parameter passing.
            main_df, cache_df = process_request(main_df, cache_df, start, end,
                subreddit, file_name, max_retries, sleep_duration, cache_limit,
                additional_params, desired_columns)

        except KeyboardInterrupt:

            print("\nKeyboardInterrupt caught, will gracefully save and exit.")

            to_csv_time = time.time()

            save(main_df, cache_df, file_name)

            print(f"It took " + format_time(to_csv_time) + "to write to CSV.")

            sys.exit()

        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):

            print("\nConnection error caught, will save and resume the process.")

            to_csv_time = time.time()

            save(main_df, cache_df, file_name)

            print(f"It took " + format_time(to_csv_time) + "to write to CSV.")

            continue

        except Exception as e:

            print("\nUnhandled exception occrred, regardless will gracefully save " \
                + "and exit.")

            to_csv_time = time.time()

            save(main_df, cache_df, file_name)

            print(f"It took " + format_time(to_csv_time) + "to write to CSV.")

            print("Unhandled error message below.")

            print(e)

            sys.exit()

'''
--------------------------------------------------------------------------------
    If you don't care about the source code and want to use the code in
    whatever working state it's in, just fill in the blanks below. It
    should work fine, I hope. - Jackie
--------------------------------------------------------------------------------
'''

start = int( dt.datetime(2020, 1, 1).timestamp() )
now = int( time.time() )
subreddit = "politics"
file_name = "politics_results_3.csv"
additional_params = {
    "link_id": "jozau7,jp0v0p,jp20uy,jp3brn,jp4e7e,jp5g6z,jp5swu,jp6nob,jp7uo2,jp9exg,jpaz19,jpc8sl,jpdbzl,jpeqf2,jpgj6e,jphhyk,jpiufx,jpk4so,jpkxmf,jpnm9j,jpr2lb,jptfkq"
}

pull_comments(start=start, end=now, subreddit=subreddit, file_name=file_name, 
    additional_params=additional_params)