# Election Live Threads

## r/conservative (note that only flaired users can comment here)
- jnle41, "Election Updates - Live Thread", 73,474 / 92,877 comments

- jousur, "Election Updates - Live Thread #2", 6,520 / 18,270 comments

## r/votedem
- jn9qrp, "[Live] 2020 Election Results Thread", 11,028 / 10,460 comments

- jnq9da, "[Live] 2020 Election Results Thread Part 2", 7,131 / 6,805 comments

## r/politics
- `politics_polls_open.csv`: jn6gd9,jnbjix,jneoqr,jnhcc9,jnjf50, "Discussion Thread: General Election 2020 - Polls Open | Part 1/.../5", 86,155 / 81,172 = (15,867 + 16,389 + 15,793 + 15,820 + 17,303) comments

- `politics_polls_close.csv`: jnkwys,jnm1r1,jnmjyj,jnn1d5,jnnhy8,jnnyms,jnovno,jnpr1b, "Discussion Thread: 2020 General Election Part 1/.../8 | <Time, State(s) Closed Polls>", 172,316 / 163,970 = (23,533 + 12,736 + 11,891 + 14,598 + 14,412 + 27,664 + 31,138 + 27,998) comments

- `politics_results_1.csv`: jnqk6j,jnrd7c,jnstmg,jnu7zd,jnvfsd,jnwf1l,jnxdj2,jnyj21,jnziid,jo0iu0,jo255b,jo3ffm,jo49cl,jo58rh,jo5z6p,jo6oh3,jo7lel,jo8cl4,jo92lv,jo9t8e,joag5a,job3dp,jobpjo, "Discussion Thread: 2020 General Election Part 9/.../31 | 11/4 <Time, State(s) Closed Polls>", 424,080 / 547,274 = (27,697 + 44,267 + 34,202 + 29,346 + 22,917 + 24,113 + 26,658 + 20,053 + 22,163 + 29,802 + 23,208 + 25,706 + 17,682 + 19,700 + 19,320 + 23,683 + 19,188 + 19,884 + 19,567 + 16,870 + 17,675 + 17,473 + 26,100)

- `politics_results_2.csv`: jocsup,joebb2,jofm3e,johfxx,joirnj,jojr3k,jokky1,jolgjk,jom8tt,jon0lo,jonu2g,joos6k,jopppj,joqnxt,jor5yo,jos2uj,jot3i4,jottzh,joucea,jov2jg,jovp08,jowd4e,jowz1f,joxkpe,joy8d9
"Discussion Thread: 2020 General Election Part 32/.../56 | 11/5 <Time, State(s) Closed Polls>"
393 / 506,678 = (30,626 + 17,934 + 18,199 + 16,355 + 17,026 + 16,903 + 17,085 + 17,303 + 18,670 + 20,035 + 17,310 + 18,006 + 19,950 + 12,802 +  21,713 +  26,042 + 25,668 + 23,902 + 20,040 + 17,658 + 20,537 + 21,012 + 20,006 + 21,265 + 30,931)

- `politics_results_3.csv`: jozau7,jp0v0p,jp20uy,jp3brn,jp4e7e,jp5g6z,jp5swu,jp6nob,jp7uo2,jp9exg,jpaz19,jpc8sl,jpdbzl,jpeqf2,jpgj6e,jphhyk,jpiufx,jpk4so,jpkxmf,jpnm9j,jpr2lb,jptfkq
"Discussion Thread: 2020 General Election Part 57/.../78 | 11/6 <Time, State(s) Closed Polls>"
X / 479,214 = (27,199 + 22,841 + 4,932 + 18,425 + 20,871 + 11,292 + 20,369 + 22,664 + 25,246 + 22,532 + 15,743 + 17,578 + 58,477 + 28,559 + 16,935 + 24,291 + 28,316 + 12,505 + 24,877 + 24,530 + 21,785 + 9,247)

## r/politicaldiscussion
- jmgv23, "2020 Election Eve Megathread", 3,574 / 3,297 comments

- jn233m, "2020 Election Day Megathread", 3,962 / 3,719 comments

- jn26hz, "2020 Presidential Election Results Megathread", 21,444 / 34,627 comments

## r/europe
- jna4o8, "United States 2020 Election - Megathread", 4,647 / 5,975 comments

## r/moderatepolitics
- jl6w35, "2020 Presidential Election Megathread - pre-Election Day Edition", 574 / 537 comments

- jn8hyl, "2020 Presidential Election Megathread - Election Boogaloo Edition, Day Drinking Hullabaloo", 4,114 / 4,082 comments

## r/neutralpolitics
- jnld4r, "2020 NeutralPolitics Election Night Megathread", 255 / 312 comments

# Other Interesting Threads

## r/politics
- jptq5n, "Megathread: Joe Biden Projected to Defeat President Donald Trump and Win the 2020 US Presidential Election", 87,426 / 83,581 comments

- jpr2lb, "Discussion Thread: 2020 General Election Part 77 | A New Hope"

- fx8hga, "Megathread: Bernie Sanders ends 2020 Democratic presidential bid", X / 71,943 comments


# Combined Live Threads for Analysis: `live_threads.csv`
Includes:

- `conservative_live_1,2.csv` 73,474 + 6,520

- `votedem_live_1,2.csv` 11,028 + 7,131

- `politicaldiscussion_live_1,2,3.csv` 3,297 + 3,719 + 21,444

- `europe_live.csv` 4,647

- `moderatepolitics_live_1,2.csv` 574 + 4,114

- `neutralpolitics_live.csv` 255

Text Only Shape:

```
Drop body NaN: 0 comments removed.
Drop [deleted]: 1998 comments removed.
Drop [removed]: 25950 comments removed.
Drop NaN: 0 comments removed.
Drop bots: 25 comments removed.
Final shape below.
(108750, 15)
```