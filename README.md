# SCUBA Lab Toolkit
**Principal Investigator:** Dr. Eshwar Chandrasekharan

**Maintainers:** Jackie Chan (PhD student), Charlotte Lambert (PhD student)

**Purpose:** To store all the common data collection and analysis tools.

# Tools
1) Pull comments from any subreddit from [Pushshift.io](https://pushshift.io/)'s Reddit API and clean it. Refer to `reddit_comment_tools` directory and respective `README.md` file for further details.

2) Pull posts from any subreddit from [Pushshift.io](https://pushshift.io/)'s Reddit API and clean it. Refer to `TODO` directory and respective `README.md` file for further details.